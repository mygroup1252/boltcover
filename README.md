# Bolt Cover Project
This is a POC for boltcover project to access how an engineer aims to streamline the deployment and management of an application by leveraging modern DevOps practices and cloud infrastructure.

## Disclaimer
This is a rough outline for a POC and does not represent actual development practices 

# Node.js Application

This segment provides a detailed step-by-step process for creating, building, running, and testing a Node.js application using Docker.

## Prerequisites
* Basic knowledge of Node.js and Docker
* Docker installed on your machine
* Node.js and npm installed on your machine

### Step 1: Create a Node.js Application
* install npm
* run `npm init -y`
* create the app.js (for this project i created a server.js to run on port 5000)
* install express using npm

### Step 2: Write the Dockerfile
* write a docker file and expose the app on port 5000
* build the image `docker build -t boltcover-app .`
* run the container to validate `docker run -d -p 5000:5000 boltcover-app`
* you should be able to access the application on `http://localhost:5000`

### Step 3: Run Unit Test
* install mocha with npm
* create the test folder
* write the unit test for the application
* install the library dependencies that are need to run mocha `esm, supertest... see the package.json`
* write the test script in package.json
* run mocha tests with `npm test` 


# CI/CD 

This segment provides a detailed step-by-step process for automating the build, test, and deployment of the Node.js application to AWS ECS using Gitlab CI.

## Prerequisites
* Basic knowledge of GitLab and AWS
* AWS account with appropriate permissions
* GitLab project repository

### Step 1: Create Identity Providers in AWS IAM
* create an AWS Identidy Provider(IdP) for gitlab using the OpenID Connect
* give the IdP assumeRole access and nessesary permission to use AWS resources
* configure the environment variables on gitlab-ci

### Step 2: Configure the Gitlab runners
* configure the GitLab Runner on docker containers using docker volumes
* register the runner with gitlab runner registration token
* once the runner is active troubleshoot it with new configuration parameters

### Step 3: Build and Push image to ECR
* create ecr repository to save our image
* use the oidc token and assume role to AWS
* build, tag and push to the ecr repository in our build stage of the gitlab-ci

### Step 4: Run Unit Tests
* pull the image to ensure we are testing the latest image in the repository
* run npm test in the test stage of our gitlab-ci
* once validated proceed to deploy

### Step 5: Deploy to ECS
* setup the ECS cluster, the task definition and the cluster service
* describe the task definition on gitlab-ci to ensure you are getting the lastest version
* create a new task definition using the lastest version of the image and register it
* update the cluster service with the new task definition 


# Provision Infrastructure

For this segment, my approach involved careful design of the modular structure for each resource.
Based on the requirements, i was able to understand that i need 5 key resources.
* VPC 
* s3 bucket
* security group
* iam
* ec2

Given the senerio, the ec2 will run within the vpc network secured with the security group, and would need access to interact with static files in the s3 bucket using IAM roles and policy.

## Creating the Modules

To ensure that the modules can be reused and maintained, I created module folders for each resource. Each module would represent its own key resource and/or set of related resources:

    EC2 Module: Responsible for creating and configuring the EC2 instance.
    
    S3 Module: Responsible for creating and configuring the S3 bucket.
    
    Security Group Module: Responsible for defining the security group and its rules.
    
    IAM Module: Responsible for creating the IAM role and attaching the necessary policies.
    
    VPC Module: Responsible for the Network

## Define Variables and Outputs

I defined input variables to parameterize the configuration, so that it can be flexible and adaptable to different environments or teams. Outputs was defined to provide useful information after the resources had been deployed (such as ids, arns and names).

## Implement the Main Configuration

The main Terraform configuration (main.tf) would orchestrate the modules, and we will be passing necessary variables values here. This centralizes the management of the infrastructure, making it easier to understand and maintain.

## Future Improvements

As this is a POC, futher improvements would be to create a gitlab-ci for autmated deploy of new resources and centerally manage from 1 point of truth. There are also there improvement we can undertake such as harderning the policy permissions writing user guides and trobleshooting steps.
