terraform {
  backend "s3" {
    bucket         = "boltcoverterraformstatebucket"
    key            = "terraform.tfstate"
    region         = "us-east-1"
  }
}