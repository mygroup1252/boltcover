module "vpc" {
  source = "./modules/vpc"
  
  vpc_cidr_block           = "10.0.0.0/16"
  public_subnet_az1_cidr   = "10.0.1.0/24" 
}

module "s3" {
  source = "./modules/s3"

  bucket_name = "boltcover-private-s3-bucket"
}

module "sg" {
  source = "./modules/sg"

  vpc_id = module.vpc.vpc_id
}

module "iam" {
  source = "./modules/iam"

  role_name = "boltcover-iam-role"
  instance_profile_name = "boltcover-instance-profile"
  policy_name = "boltcover-iam-policy"
}

module "ec2" {
  source = "./modules/ec2"

  ami_id = "ami-04b70fa74e45c3917"
  associate_public_ip = true
  type = "t2.micro"
  key_name = "My-Machine-KP"
  subnet_id = module.vpc.public_subnet_az1_id
  security_group_ids = [module.sg.ec2_security_group_id]
  iam_instance_profile = module.iam.instance_profile_name
}