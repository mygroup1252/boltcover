resource "aws_security_group" "ec2_security_group" {
  name        = "sec_group"
  description = "security group for the EC2 instance"
  vpc_id      = var.vpc_id

  ingress {
    description      = "http traffic"
    from_port        = 80
    to_port          = 80
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
  }

  ingress {
    description      = "ssh"
    from_port        = 22
    to_port          = 22
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    description      = "Outbound traffic rule"
  }

  tags = {
    name = "boltcover-terraform-sg"
  }
}

