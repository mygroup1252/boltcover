variable "role_name" {
  description = "Name for the Role."
  type        = string
}

variable "service" {
  description = "AWS Service which will assume the Role. Must be supplied if you are not creating an ADFS Role."
  default     = null
  type        = string
}

variable "description" {
  description = "Description for the Role."
  default     = null
  type        = string
}

variable "instance_profile_name" {
  description = "Name for the Instance profile."
  type        = string
}

variable "policy_name" {
  description = "Name for the Policy."
  type        = string
}

