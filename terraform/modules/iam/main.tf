
# create iam role
resource "aws_iam_role" "role" {
  name = var.role_name

  assume_role_policy = data.aws_iam_policy_document.instance_assume_role_policy.json
  
  tags = {
    Name = "boltcover-terraform-iam-role"
  }
}

# create instance profile
resource "aws_iam_instance_profile" "role_instance_profile" {
  name = var.instance_profile_name
  role = aws_iam_role.role.name

  tags = {
    Name = "boltcover-terraform-iam-instance-profile"
  }
}

# create iam policy for s3 access
resource "aws_iam_role_policy" "policy" {
  name = var.policy_name
  role = aws_iam_role.role.id

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "s3:*"
      ],
      "Effect": "Allow",
      "Resource": "*"
    }
  ]
}
EOF
}