

output "arn" {
  description = "ARN of the Role."
  value = aws_iam_role.role.arn
}

output "id" {
  description = "ID of the Role."
  value = aws_iam_role.role.id
}

output "name" {
  description = "Name of the Role."
  value = aws_iam_role.role.name
}

output "instance_profile_arn" {
  description = "ARN of Role Instance Profile."
  value = aws_iam_instance_profile.role_instance_profile.arn
}

output "instance_profile_name" {
  description = "ARN of Role Instance Profile."
  value = aws_iam_instance_profile.role_instance_profile.name
}

output "policy_id" {
  description = "Id of Policy."
  value = aws_iam_role_policy.policy.id
}