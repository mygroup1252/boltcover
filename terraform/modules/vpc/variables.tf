variable "vpc_cidr_block" {
 description = "CIDR block for the VPC"
 type        = string
}

variable "public_subnet_az1_cidr" {
 description = "CIDR block for the public subnet in AZ1"
 type        = string
}
