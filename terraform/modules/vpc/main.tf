# Create vpc
resource "aws_vpc" "vpc" {
 cidr_block = var.vpc_cidr_block

 tags = {
   Name = "boltcover-terraform-vpc"
 }
}

# use data source to get all avalablility zones in region
data "aws_availability_zones" "available_zones" {}

# create public subnet on az1
resource "aws_subnet" "public_subnet_az1" {
 vpc_id                  = aws_vpc.vpc.id
 cidr_block              = var.public_subnet_az1_cidr
 availability_zone       = data.aws_availability_zones.available_zones.names[0]
 map_public_ip_on_launch = true

 tags = {
   name = "boltcover-terraform-public-subnet-az1"
 }
}

# Create an internet gateway and attach to vpc for public access to the internet
resource "aws_internet_gateway" "igw" {
 vpc_id = aws_vpc.vpc.id

 tags = {
   name = "boltcover-terraform-igw"
 }
}

# Create a custom route table
resource "aws_route_table" "route_table" {
 vpc_id = aws_vpc.vpc.id

 tags = {
   name = "boltcover-terraform-route-table"
 }
}

# create route
resource "aws_route" "route" {
 destination_cidr_block = "0.0.0.0/0"
 gateway_id             = aws_internet_gateway.igw.id
 route_table_id         = aws_route_table.route_table.id
}

# associate subnet to route table
resource "aws_route_table_association" "subnet_az1_association" {
 subnet_id      = aws_subnet.public_subnet_az1.id
 route_table_id = aws_route_table.route_table.id
}
