output "vpc_id" {
 description = "ID of the created VPC"
 value       = aws_vpc.vpc.id
}

output "public_subnet_az1_id" {
 description = "ID of the public subnet in AZ1"
 value       = aws_subnet.public_subnet_az1.id
}

output "internet_gateway_id" {
 description = "ID of the created internet gateway"
 value       = aws_internet_gateway.igw.id
}

output "route_table_id" {
 description = "ID of the created route table"
 value       = aws_route_table.route_table.id
}
