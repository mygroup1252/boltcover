
# create ec2
resource "aws_instance" "instance" {
  ami = var.ami_id
  associate_public_ip_address = var.associate_public_ip
  instance_type = var.type
  key_name = var.key_name
  monitoring = true
  private_ip = var.private_ip
  subnet_id = var.subnet_id
  vpc_security_group_ids = var.security_group_ids
  iam_instance_profile = var.iam_instance_profile

  tags = {
    Name = "boltcover-terraform-ec2-instance"
  }
}