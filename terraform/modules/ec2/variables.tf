
variable "ami_id" {
  description = "The AMI ID that will be set for the image"
  type        = string
}

variable "associate_public_ip" {
  description = "Whether the instance is provided with a public IP."
  default = false #switch to true on root main.tf
  type = bool
}

variable "type" {
  description = "Compute type to deploy. See https://aws.amazon.com/ec2/instance-types/ for more information."
  type = string
}

variable "key_name" {
  description = "Name of EC2 private key-pair used for SSH."
  default = null
  type  = string
}

variable "private_ip" {
  description = "Private IP to assign to the instance. If left blank AWS will assign an IP for you."
  default = null
  type = string
}

variable "subnet_id" {
  description = "ID of VPC Subnet where instance will be deployed."
  type  = string
}

variable "security_group_ids" {
  description = "List of Security Group IDs for the instance. SGs must be created in same VPC as subnet_id."
  type  = list(string)
}

# variable "iam_role_policy_documents" {
#   description = "List of IAM Policy Documents in JSON format that will be assigned to Role."
#   default     = null
#   type        = list(string)
# }

variable "iam_instance_profile" {
  description = "Name for the instance profile."
  type        = string
}







# variable "user_data" {
#   description = "EC2 UserData that will be ran on the instance upon initialisation."
#   type = string
#   default = null
# }
