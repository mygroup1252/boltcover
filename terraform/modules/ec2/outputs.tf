output "arn" {
  description = "Name of instance."
  value = aws_instance.instance.arn
}

output "public_ip" {
  description = "Name of instance."
  value = aws_instance.instance.public_ip
}

# output "role_name" {
#   description = "Name of Role assigned to instance."
#   value = module.instance_role.name
# }
# 
# output "role_arn" {
#   description = "ARN of Role assigned to instance."
#   value = module.instance_role.arn
# }
