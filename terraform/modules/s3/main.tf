# create a s3 bucket
resource "aws_s3_bucket" "s3_bucket" {
  bucket = var.bucket_name
  force_destroy = true

  tags = {
    Name = "boltcover-terraform-s3-bucket"
  }
}

resource "aws_s3_bucket_ownership_controls" "bucker_owner" {
  bucket = aws_s3_bucket.s3_bucket.id

  rule {
    object_ownership = "BucketOwnerPreferred"
  }
}

resource "aws_s3_bucket_acl" "bucket_acl" {
  depends_on = [aws_s3_bucket_ownership_controls.bucker_owner]
  
  bucket = aws_s3_bucket.s3_bucket.id
  acl = "private"
}

# create life-cycle configuration
# resource "aws_s3_bucket_lifecycle_configuration" "lifecycle" {
#     bucket = aws_s3_bucket.s3_bucket.id
# 
#     rule {
#         id = "archival"
#         filter {
#           and {
#             prefix = "/"
#             tags = {
#               rule      = "archival"
#               autoclean = "false"
#             }
#           }
#         }    
#         status = "Enabled"
#         transition {
#           days          = 30
#           storage_class = "STANDARD_IA"
#         }
#         transition {
#           days          = 60
#           storage_class = "GLACIER"
#         }
#     }
# }
